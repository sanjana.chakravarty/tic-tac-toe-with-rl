# Tic Tac Toe with RL

**Objective** : Training a tic tac toe game with reinforcement learning to improve the AI’s success rate with experience.

**Reinforcement learning** (RL) is an area of machine learning concerned with how software agents ought to take actions in an environment in order to maximize the notion of cumulative reward. By exploring its environment and exploiting the most rewarding steps, it learns to choose the best action at each stage.
We are planning to implement the grid game by iteratively updating **Q value** function, which is the estimating value of (state, action) pair.

**Language**: Python and Python modules (pygame etc)

**Dependencies**: Need to install pygame and pygame-menu Python modules in order to run the code.

**Usage**: Clone the repository and run the tictactoe.py file to play with the agent interactively. To first train the agent, enter 'Y' on prompt, else enter 'n'. 
